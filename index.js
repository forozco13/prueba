const express = require('express');
const app = express();
const { graphqlHTTP } = require('express-graphql');
const {  root } = require('./src/api');
const { schemaCities } = require('./src/schema');

app.use('/search', graphqlHTTP({
    schema: schemaCities,
    rootValue: root,
    graphiql: true
}));

app.listen(3000, () => console.log('server on port 3000'));