var _ = require('lodash');
var score = require('string-score');
const { cities } = require('./data/data.json');


getCities = (args) => {
    if (args.q) {
        let q = args.q.trim().toLowerCase();
        let long = args.longitude ? args.longitude.trim() : null;
        let lat = args.latitude ? args.latitude.trim() : null;
        let arrFilteredCities = [];

        if (long != null && lat != null) {
            arrFilteredCities = cities.filter(city => {
                return ((city.lat === lat && city.long === long) || (city.name.toLowerCase().includes(q) || city.alt_name.toLowerCase().includes(q)) )
            });
        }

        if (long == null && lat == null) {
            arrFilteredCities = cities.filter(city => {
                return ((city.name.toLowerCase().includes(q) || city.alt_name.toLowerCase().includes(q)))
            });
        }

        arrFilteredCities.map(city => {
            city.score = score(city.name, q).toFixed(2);
        });
        
        return _.orderBy(arrFilteredCities, 'score', 'desc');
    }
};

exports.root = {
    cities: getCities
};