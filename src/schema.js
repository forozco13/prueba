const { buildSchema } = require('graphql');

exports.schemaCities = buildSchema(`
    type Query {
        cities(q: String !, latitude: String, longitude: String): [City]
    }

    type City {
        id: String,
        name: String,
        ascii: String,
        alt_name: String,
        lat: String,
        long: String,
        feat_class: String,
        feat_code: String,
        country: String,
        cc2: String,
        admin: String,
        admin2: String,
        admin3: String,
        admin4: String,
        population: String,
        elevation: String,
        dem: String,
        tz: String,
        modified_at: String,
        score: Float
    }

`);